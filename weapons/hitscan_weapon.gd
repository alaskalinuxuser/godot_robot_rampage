extends Node3D

@onready var audio_stream_player: AudioStreamPlayer = $AudioStreamPlayer
@onready var reloading_timer: Timer = $ReloadingTimer
@onready var animation_player: AnimationPlayer = $AnimationPlayer
@onready var cooldown_timer: Timer = $CooldownTimer
@export var fire_rate := 14.0
@export var recoil := 0.05
@export var kick := 0.15
@export var weapon_damage := 15
@export var weapon_mesh := Node3D
@export var muzzle_flash : GPUParticles3D
@export var sparks_scene : PackedScene
@export var automatic : bool
@export var ammo_handler : ammoHandler
@export var ammo_type: ammoHandler.ammo_type
@onready var weapon_position : Vector3 = weapon_mesh.position
@onready var weapon_rotation : Vector3 = weapon_mesh.rotation
@onready var ray_cast_3d: RayCast3D = $RayCast3D
@onready var reloading := false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if automatic:
		if Input.is_action_pressed("fire") :
			if cooldown_timer.is_stopped() && !reloading:
				shoot()
	else:
		if Input.is_action_just_pressed("fire") :
			if cooldown_timer.is_stopped() && !reloading:
				shoot()

	weapon_mesh.position = weapon_mesh.position.lerp(weapon_position, delta*10.0)
	weapon_mesh.rotation = weapon_mesh.rotation.lerp(weapon_rotation, delta*10.0)
	
	if Input.is_action_just_pressed("reload"):
		reload_weapon()

func shoot() -> void:
	if ammo_handler.has_ammo(ammo_type):
		ammo_handler.use_ammo(ammo_type)
		audio_stream_player.play()
		muzzle_flash.restart()
		cooldown_timer.start(1.0/fire_rate)
		weapon_mesh.position.z += recoil
		weapon_mesh.rotation.x += kick
		animation_player.play("fire")
		var collider = ray_cast_3d.get_collider()
		#print(collider)
		if ray_cast_3d.is_colliding():
			if collider is Enemy:
				collider.hit_points -= weapon_damage
			
			var spark = sparks_scene.instantiate()
			add_child(spark)
			spark.global_position = ray_cast_3d.get_collision_point()

func reload_weapon() -> void:
	reloading = true
	animation_player.play("reload")
	reloading_timer.start()
	
func reloaded() -> void:
	reloading = false
