extends Node3D
class_name ammoHandler

@export var ammo_label: Label
@export var weapon_handler: Node3D

enum ammo_type {
	CBK_BULLET,
	SMG_BULLET,
	RIFLE_BULLET
}

var ammo_storage := {
	ammo_type.CBK_BULLET: 6,
	ammo_type.SMG_BULLET: 20,
	ammo_type.RIFLE_BULLET: 10
}

func has_ammo(type: ammo_type) -> bool:
	return ammo_storage[type] > 0
	

func use_ammo(type: ammo_type) -> void:
	if has_ammo(type):
		ammo_storage[type] -= 1
		update_ammo_label(weapon_handler.get_weapon_ammo())
		
func add_ammo(type: ammo_type, ammount: int) -> void:
	ammo_storage[type] += ammount
	# This doesn't work quite right. If you pick up ammo of the other guns, it will update your current gun lable.
	update_ammo_label(weapon_handler.get_weapon_ammo())

func update_ammo_label(type: ammo_type) -> void:
	ammo_label.text = str(ammo_storage[type])
