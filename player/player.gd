extends CharacterBody3D
#class_name Playerww

@export var speed = 6.0
@export var jump_height: float = 1.0
@export var fall_multiplyer: float = 2.5
@export var max_health := 100
@export var aim_multiplier := 0.7
var unlocked_door = false

var hit_points : int = max_health:
	set(value):
		if hit_points > value:
			damage_animation.stop(false)
			damage_animation.play("damage")
		hit_points = value
		if hit_points <= 0:
			game_over_menu.game_over()

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")
var mouse_motion := Vector2.ZERO
@onready var camerapivot: Node3D = $camerapivot
@onready var damage_animation: AnimationPlayer = $DamageTexture/damageAnimation
@onready var game_over_menu: Control = $GameOverMenu
@onready var ammo_handler: ammoHandler = %AmmoHandler
@onready var smooth_camera: Camera3D = %SmoothCamera
@onready var smooth_camera_fov:= smooth_camera.fov
@onready var weapon_camera: Camera3D = %WeaponCamera
@onready var weapon_camera_fov:= weapon_camera.fov
@onready var win_menu: Control = $GameOverMenu2
@onready var hero: Node3D = $hero


func _ready() -> void:
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	
func _process(_delta: float) -> void:
	#if Input.is_action_pressed("aim"):
		#smooth_camera.fov = lerp(smooth_camera.fov, smooth_camera_fov * aim_multiplier, delta * 20)
		#weapon_camera.fov = lerp(weapon_camera.fov, weapon_camera_fov * aim_multiplier, delta * 20)
	#else:
		#smooth_camera.fov = lerp(smooth_camera.fov, smooth_camera_fov, delta * 40)
		#weapon_camera.fov = lerp(weapon_camera.fov, weapon_camera_fov, delta * 40)
	
	if Input.is_action_just_pressed("aim"):
		smooth_camera.fov = smooth_camera_fov * aim_multiplier
		weapon_camera.fov = weapon_camera_fov * aim_multiplier
	if Input.is_action_just_released("aim"):
		smooth_camera.fov = smooth_camera_fov
		weapon_camera.fov = weapon_camera_fov

func _physics_process(delta: float) -> void:
	handle_camera_rotation()
	# Add the gravity.
	if not is_on_floor():
		if velocity.y >= 0:
			velocity.y -= gravity * delta
		else:
			velocity.y -= gravity * delta * fall_multiplyer

	# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = sqrt(jump_height * 2.0 * gravity)

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir := Input.get_vector("move_left", "move_right", "move_forward", "move_back")
	var direction := (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
		if Input.is_action_pressed("aim"):
			velocity.x *= aim_multiplier
			velocity.y *= aim_multiplier
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
		velocity.z = move_toward(velocity.z, 0, speed)

	move_and_slide()

func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
			mouse_motion = -event.relative * 0.001
			if Input.is_action_pressed("aim"):
				mouse_motion *= aim_multiplier
	if event.is_action("ui_cancel"):
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func handle_camera_rotation() -> void:
	rotate_y(mouse_motion.x)
	camerapivot.rotate_x(mouse_motion.y)
	camerapivot.rotation_degrees.x = clampf(
		camerapivot.rotation_degrees.x, -90.0, 90.0
	)
	hero.rotate_x(mouse_motion.y)
	mouse_motion = Vector2.ZERO

func you_win() -> void:
	win_menu.win()
