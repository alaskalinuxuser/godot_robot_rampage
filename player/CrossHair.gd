extends Control

var reticle_color = Color.WHITE
var reticle_outline = Color.GRAY
var reticle_start = 14
var reticle_end = 20

func _draw() -> void:
	draw_circle(Vector2.ZERO, 3, reticle_outline)
	draw_circle(Vector2.ZERO, 2, reticle_color)
	
	draw_line(Vector2(reticle_start,0),Vector2(reticle_end,0), reticle_outline, 4, false)
	draw_line(Vector2(reticle_start,0),Vector2(reticle_end,0), reticle_color, 2, false)
	
	draw_line(Vector2(-reticle_start,0),Vector2(-reticle_end,0), reticle_outline, 4, false)
	draw_line(Vector2(-reticle_start,0),Vector2(-reticle_end,0), reticle_color, 2, false)
	
	draw_line(Vector2(0,reticle_start),Vector2(0,reticle_end), reticle_outline, 4, false)
	draw_line(Vector2(0,reticle_start),Vector2(0,reticle_end), reticle_color, 2, false)
	
	draw_line(Vector2(0,-reticle_start),Vector2(0,-reticle_end), reticle_outline, 4, false)
	draw_line(Vector2(0,-reticle_start),Vector2(0,-reticle_end), reticle_color, 2, false)
