extends Control

@onready var audio_stream_player: AudioStreamPlayer = $AudioStreamPlayer

func win() -> void:
	get_tree().paused = true
	visible = true
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	audio_stream_player.play()

func _on_quit_button_pressed() -> void:
	get_tree().quit()

func _on_play_again_button_pressed() -> void:
	get_tree().paused = false
	get_tree().reload_current_scene()
