# Robot Rampage!

A simple 3D first person shooter game designed in Godot while taking a Udemy class.

The game consists of only one level, where you attempt to get from the entrance door to the exit door. The door will not open until you activate it from the computer console.

This is not a polished game. Just a tool for learning.

The controls the typical FPS WASD keys, spacebar to jump, and use the mouse left button to shoot, right button to zoom, center wheel to cycle through the weapons.

Using Godot, this game may be built for HTML5, Windows, and Linux.

The class I was part of did have models and artwork, however, I made all of the artwork and models used in my version of the game for learning purposes. The fonts came from Ubuntu's repository, and the audio came from freesound.org and I can only say sorry ahead of time for my bulky 3D models.
