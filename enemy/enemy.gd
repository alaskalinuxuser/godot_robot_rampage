extends CharacterBody3D
class_name Enemy

@onready var navigation_agent_3d: NavigationAgent3D = $NavigationAgent3D
@onready var animation_tree: AnimationTree = $AnimationTree
@onready var playback: AnimationNodeStateMachinePlayback = animation_tree["parameters/playback"]
@onready var explosion_timer: Timer = $ExplosionTimer
@onready var explosion_particles: GPUParticles3D = $ExplosionParticles
@onready var audio_stream_player: AudioStreamPlayer = $AudioStreamPlayer
@onready var attack_audio: AudioStreamPlayer = $AttackAudio

@export var speed = 5.0
@export var explosion_scene : PackedScene
@export var aggro_range := 12.0
@export var attack_range := 1.5
@export var max_health := 100
var player
var provoked := false
var attacking := false
var not_exploding := true
var hit_points : int = max_health:
	set(value):
		hit_points = value
		if hit_points <= 0:
			hit_points = 1000
			die_already()
		provoked = true

func die_already() -> void:
	explosion_particles.restart()
	audio_stream_player.play()
	explosion_timer.start()
	speed = 0.0
	not_exploding = false

func _ready() -> void:
	player = get_tree().get_first_node_in_group("player")

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")

func _process(_delta: float) -> void:
	if provoked :
		navigation_agent_3d.target_position = player.global_position
		if attacking :
			playback.travel("attacking")

func _physics_process(delta: float) -> void:
	var next_postion = navigation_agent_3d.get_next_path_position()
	
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
	
	var direction = global_position.direction_to(next_postion)
	var distance = global_position.distance_to(player.global_position)
	
	if distance <= aggro_range :
		provoked = true
		if distance <= attack_range and not_exploding :
			attacking = true
		else :
			attacking = false
		# Could have an else statement to set to false when out of range, but not useful here.
	
	if direction:
		look_at_target(direction)
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
		velocity.z = move_toward(velocity.z, 0, speed)

	move_and_slide()

func look_at_target (direction: Vector3) -> void:
	var adjusted_direction = direction
	adjusted_direction.y = 0
	look_at(global_position+adjusted_direction, Vector3.UP, true)

func attack_target():
	#print ("Attack!")
	attack_audio.play()
	player.hit_points -= 20


func _on_explosion_timer_timeout() -> void:
	queue_free()
