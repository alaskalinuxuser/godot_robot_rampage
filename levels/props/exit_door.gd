extends Area3D

@onready var audio_denied: AudioStreamPlayer = $audio_denied

var player

func _ready() -> void:
	player = get_tree().get_first_node_in_group("player")
	
func _on_body_entered(body: Node3D) -> void:
	if body.is_in_group("player"):
		if body.unlocked_door:
			player.you_win()
			# go to next level, etc.
		else:
			audio_denied.play()
