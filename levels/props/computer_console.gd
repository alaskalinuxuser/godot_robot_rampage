extends Node3D

@onready var red_button: MeshInstance3D = $Bottom/Top/RedButton
@onready var green_button: MeshInstance3D = $Bottom/Top/GreenButton
@onready var button_timer: Timer = $buttonTimer
@onready var audio_stream_player: AudioStreamPlayer = $AudioStreamPlayer

var first_try := true

func _ready() -> void:
	red_button.visible = true
	green_button.visible = false

func _on_button_timer_timeout() -> void:
	red_button.visible = false
	green_button.visible = true
	audio_stream_player.play()

func _on_area_3d_body_entered(body: Node3D) -> void:
	if body.is_in_group("player") and first_try:
		button_timer.start()
		body.unlocked_door = true
		first_try=false
