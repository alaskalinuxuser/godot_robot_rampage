extends Area3D


@export var ammoType: ammoHandler.ammo_type
@export var ammount:= 20



func _on_body_entered(body: Node3D) -> void:
	if body.is_in_group("player"):
		body.ammo_handler.add_ammo(ammoType, ammount)
		queue_free()
